# Description

This utility allows you to encrypt and rename 'Strings type' into a VBNET (*.vbproj) project. The program will generate functions throughout your loaded project.


# Features

* French UI language only
* Displays the project's files (not ".designer." files !)
* Displays source code with syntax highlighting 
* Displays the found String occurences with their line numbers
* Selecting encryption method (Base64, Xor, and Both) with Ascii string conversion.
* Displays a progress windows when loading and searching the String type occurences throughout the selected file.


# Prerequisites

* Compatible VbNet projects developped with VS2010 or VS2012 EDI
* All Windows OS
* DotNet Framework 4.0
* Doesn't require installation


# WebSite

* [http://3dotdevcoder.blogspot.fr/](http://3dotdevcoder.blogspot.fr/)


# Copyright

* Attribution
* NonCommercial
* ShareAlike 4.0 International License.


# Licence
![Attribution-NonCommercial-ShareAlike 4.0 International License.](http://imabox.fr/a2/1414022011wFgy4F45.png "Creative Commons")